# TP GIT #2 - Git avancé

> **Objectifs du TP**
> * Effectuer un git stash
> * Effectuer un git rebase et gérer les conflits
> * Créer votre première merge request

> 
> **Prérequis**
> Avant de commencer ce TP, vous devez avoir satisfait les prérequis suivants
> * Vous avez validé votre accès à Gitlab
> * Vous avez ajouté votre clé SSH a votre compte
> 
> **Niveau de difficulté :** Intermédiaire

# Fork d'un projet git

- Connectez-vous, si ce n'est pas déjà fait, sur votre compte gitlab.com
- Aller sur le dépôt [https://gitlab.com/farcellier/page-web-participative-rebase](https://gitlab.com/farcellier/page-web-participative-rebase)
- Forkez ce dépôt en cliquant sur le bouton `Fork`
- Visualiser le dépôt forké sur gitlab
- Cloner le dépôt sur votre poste de travail

Depuis un terminal :
```bash
$ git clone git@gitlab.com:<votre pseudo gitlab>/page-web-participative-rebase.git
```


# Mise sur "étagère" du code avec `git stash`

Parfois, vous souhaitez mettre votre travail de côté pour travailler sur une autre sujet et le reprendre plus tard et ainsi de repartir d'un environnement de travail propre.

Dans ce cas, `git stash` est le bon candidat pour conserver du travail temporaire, et évite de commiter des fichiers non testés.

Simulons maintenant des modifications dans notre dossier de travail : 

```bash
$ sed -e 's/Your Favorite Source of Free Bootstrap Themes/The best Free Boostrap Theme/g' index.html
```

Cette commande permet juste de remplacer la chaîne de caractère `Your Favorite Source of Free Bootstrap Themes` par `The best Free Boostrap Theme`.

Vous pouvez, si le souhaitez modifier un ou plusieurs autres fichiers.

Nous allons maintenant mettre sur étagère les modifications avec la commande `git stash`

```bash
# juste pour voir le(s) fichier(s) modifié(s)
$ git status

$ git stash

# vérification qu'aucune modification n'apparaît
$ git status
```

De plus, vous pouvez afficher le contenu du fichier modifié pour voir que les modifications ont réellement "disparues" de l'espace de travail

```bash
$ cat index.html
```

Pour "restaurer" vos modifications, vous pouvez exécuter la commande `git stash pop`

```bash
$ git stash pop

# vérification pour voir que le(s) fichier(s) modifié(s) réapparaissent
$ git status
```

# Annuler les modifications précédentes

Nous souhaitons finalement annuler le remplacement de texte que nous avons effectué dans l'étape précédente. Pour cela, nous allons utiliser les commandes :
- `git reset index.html` pour indiquer à git de ne plus considéré le fichier index.html comme "dans la liste des fichiers à prendre dans le prochain commit"
- `git checkout index.html` pour annuler les modifications effectuées en récupérant le fichier tel qu'il était lors du dernier commit

```bash
$ git reset index.html
$ git checkout index.html
```

L'espace de travail doit être maintenant vierge : 

```bash
$ git status
nothing to commit, working tree clean
```

# Exécution d'un `git rebase`

- Se positionner dans l'espace de travail où vous avez cloné votre `fork` du dépôt `page-web-participative-rebase`
- Afficher en console les 2 branches qui divergent

```bash
$ git log --graph --oneline --decorate origin/master origin/feat-new_button-farcellier
```

- Se positionner sur la branche `feat-new_button-farcellier`

```bash
$ git checkout feat-new_button-farcellier
```

- Lancer la commmande rebase sur la branche `master`

```bash
$ git rebase master
```

Vous devriez avoir un résultat similaire à celui-ci:
```bash
$ git rebase master
First, rewinding head to replay your work on top of it...
Applying: feat: the marketing requests we add a button to send an email
Applying: feat: the marketing request we rename the button
```

> Remarque, la commande `git pull --rebase origin master`, exécuté depuis la branche `feat-new_button-farcellier`, est un raccourci sur les 2 dernières commandes que nous venons d'exécuter : `git checkout feat-new_button-farcellier` et `git rebase master`

- Pour propager l'opération de `rebase` effectué en local sur le dépôt gitlab, nous allons maintenant exécuter la commande `git push`

```bash
$ git push origin feat-new_button-farcellier
```

Pas de chance, vous obtenez probablement une erreur similaire à celle-ci : 

```bash
$ git push origin feat-new_button-farcellier
Warning: Permanently added 'gitlab.com,35.231.145.151' (ECDSA) to the list of known hosts.
To gitlab.com:ylascombe-octo/page-web-participative-rebase.git
 ! [rejected]        feat-new_button-farcellier -> feat-new_button-farcellier (non-fast-forward)
error: failed to push some refs to 'git@gitlab.com:ylascombe-octo/page-web-participative-rebase.git'
hint: Updates were rejected because the tip of your current branch is behind
hint: its remote counterpart. Integrate the remote changes (e.g.
hint: 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details
```

> Pourquoi obtenons une erreur ? Savez-vous l'expliquer ?

En fait cette erreur est une sécurité de Git : comme l'opération `rebase` a réécrit l'histoire des commits, cela signifie que nous avons altéré cette même histoire.
Pour propager les modifications sur le dépôt Gitlab, Git nous demande donc de "forcer" la commande en ajoutant l'option `--force ` (ou `-f` pour ceux qui comme nous essaient de minimiser la saisie clavier)

Ré-essayons maintenant de pousser nos modifications avec l'option `--force`

```
$ git push origin feat-new_button-farcellier --force
```

La commande doit maintenant aboutir sans erreur.

# Exécution d'un `git rebase` avec une résolution de conflits à effectuer

- Dans un nouveau dossier, clonez le dépôt https://gitlab.com/farcellier/page-web-participative-rebase-conflict (attention, même si le nom du dépôt est indentique à l'étape précédente, ce dépôt correspond pas au "fork" mais au dépôt initial, pensez donc à vous mettre dans un dossier de travail différent)

```bash
$ git clone https://gitlab.com/farcellier/page-web-participative-rebase-conflict
$ cd page-web-participative-rebase-conflict
```

- S'assurez que vous êtes bien positionné sur la branche `master`

```bash
$ git branch
```

- Effectuez un rebase de la branche feat-new_button-farcellier

```bash
$ git rebase origin/feat-new_button-farcellier
```

> Question : pourquoi nous avons préfixé le nom de la branche par `origin/` ?
>
> Ce paramètre est en effet optionnel. Si nous l'ométtons, git recherchera donc la branche `feat-new_button-farcellier` sur l'espace de travail local. Comme nous ne l'avons pas encore `checkout`, git ne la trouvera donc pas sur la commande `git rebase`. Il faut donc au préalable l'avoir `checkout` avec la commande `git checkout feat-new_button-farcellier` pour pouvoir omettre le mot `origin/`.

Vous devriez avoir un résultat similaire à celui-ci:
```bash
$ git rebase feat-new_button-farcellier
First, rewinding head to replay your work on top of it...
Applying: docs: improve readme with contributing rules
Applying: feat: marketing requests to improve visual
Applying: refact: add intellij ide in the gitignore file
Applying: feat: market request we had a button product
Using index info to reconstruct a base tree...
M	index.html
Falling back to patching base and 3-way merge...
Auto-merging index.html
CONFLICT (content): Merge conflict in index.html
error: Failed to merge in the changes.
Patch failed at 0004 feat: market request we had a button product
hint: Use 'git am --show-current-patch' to see the failed patch
Resolve all conflicts manually, mark them as resolved with
"git add/rm <conflicted_files>", then run "git rebase --continue".
You can instead skip this commit: run "git rebase --skip".
To abort and get back to the state before "git rebase", run "git rebase --abort".
```

Cette fois-ci, le rebase n'a pu donc se faire tout seul, ici, git nous indique qu'il y a des conflits et que nous devons intervenir pour les résoudre.

En regardant le message d'erreur de plus près, on peut voir que le conflit de se situe dans le fichier `index.html`.

- Résoudre le conflit dans le fichier index.html : Utilisez maintenant votre éditeur de texte favori pour le résoudre. 

Concrètement, pour trouver les conflits dans le fichier concerné, il suffit de rechercher les chaines `<<<<<<<`, `=======` et `>>>>>>>`. Exemple pour notre conflit

```bash
$ grep '<<<<' index.html -C 6
        <div class="header-content">
            <div class="header-content-inner">
                <h1 id="homeHeading">Your Favorite Source of Free Bootstrap Themes</h1>
                <hr>
                <p>Start Bootstrap can help you build better websites using the Bootstrap CSS framework! Just download your template and start going, no strings attached!</p>
                <a href="#about" class="btn btn-primary btn-xl page-scroll">Find Out More</a>
<<<<<<< HEAD
                <a href="#about" class="btn btn-primary btn-xl page-scroll">Contact us</a>
=======
                <a href="#about" class="btn btn-primary btn-xl page-scroll">Our product</a>
>>>>>>> feat: market request we had a button product
            </div>
        </div>
```

> Résolution des conflits :
> - Tout le texte qui se situe entre `<<<<<<< HEAD` et `=======` représente les modifications actuelles (sur votre espace de travail)
> - Tout le texte qui se situe entre `=======` et `>>>>>>>` représente les modifications entrante 
>
> La technique consiste donc à choisir le code qui restera après le merge et à retirer les marqueurs `<<<<<<<`, `=======` et `>>>>>>>`

Ici, nous souhaitons conserver les 2 morceaux de code, le code après merge sera donc:

```html
        <div class="header-content">
            <div class="header-content-inner">
                <h1 id="homeHeading">Your Favorite Source of Free Bootstrap Themes</h1>
                <hr>
                <p>Start Bootstrap can help you build better websites using the Bootstrap CSS framework! Just download your template and start going, no strings attached!</p>
                <a href="#about" class="btn btn-primary btn-xl page-scroll">Find Out More</a>
                <a href="#about" class="btn btn-primary btn-xl page-scroll">Contact us</a>
                <a href="#about" class="btn btn-primary btn-xl page-scroll">Our product</a>
            </div>
        </div>
```

Dans un IDE graphique, la résolution de conflits se fait selon la technique des 3 ways merge. Exemple dans IntelliJ :
![](./.assets/git-3-ways-merge.png)

Le fichier résultat est celui du milieu. Pour chaque changement, il faut indiquer si on prend, on ignore ou on prend en faisant des modifications.

- Une fois que le conflit est résolu en local, continuons le rebase en cours:

```bash
$ git rebase --continue
```

Vous vous demandez probablement pourquoi nous n'avons pas "commiter" la résolution du conflit avant d'effectuer la commande `git rebase --continue`. Il se trouve que git effectue le commit pour nous en modifiant le commit qu'il était en train de "rejouer" pour le rebase avec la nouvelle version des fichiers qui ont générés des conflits.
Le commit reproduit ne sera donc plus exactement le même qu'initialement, l'histoire a été réécrite et légèrement modifiée.

La commande `rebase --continue` doit produire un résultat similaire à celui-ci : 

```bash
$ git rebase --continue
Applying: feat: market request we had a button product
```

À ce stade, nous avons maintenant terminé le rebase avec résolution de conflits.

> Un dernier conseil, lors de l'exécution d'un `rebase` avec conflit, il n'est pas rare de faire une erreur les premières fois, dans ce cas, la commande `git rebase --abort` vous permet d'abandonner le `rebase` et de remettre votre dossier de travail tel qu'il était avant la commande `git rebase`.

Ce TP est maintenant terminé. 
