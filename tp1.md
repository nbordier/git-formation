# TP GIT #1 - Création compte gitlab.com et configuration Git en local

> **Objectifs du TP**
> * Créer un premier dépôt Git et le partager sur gitlab.com
>
> **Niveau de difficulté :** Débutant

# Création d’un premier repo git

Exécuter les commandes suivantes
```bash
$ cd ~/{{ tags.Student }}/
$ mkdir monrepo
$ cd monrepo
$ git init
```
La commande `git init` permet d'initialiser votre dépôt Git.

On ajoute un fichier et on le commit…
```bash
$ touch readme.txt
$ git add readme.txt
$ git status
$ git commit -m "added read me"
$ git status
$ git log
```

> `git add .` ou `git add –all` aurait aussi fonctionné pour ajouter l’ensemble des fichiers du dépôt

Résultat de la commande `git log`:
```bash
commit 99b0bcb0b334123005aba2bdba858970c88abf1b (HEAD -> master)
Author: Yohan Lascombe <ylascombe@octo.com>
Date:   Sun Dec 15 23:55:05 2019 +0100

    added read me
```

> Questions avancées
>
> - La commande `git init --bare` crée un repository central. À quoi sert ce type de repo. ?
> - Qui a le même hash de commit que moi ?
>
> Réponses:
> - [https://git-scm.com/book/fr/v2/Git-sur-le-serveur-Mise-en-place-du-serveur](https://git-scm.com/book/fr/v2/Git-sur-le-serveur-Mise-en-place-du-serveur)
> - [https://blog.thoughtram.io/git/2014/11/18/the-anatomy-of-a-git-commit.html](https://blog.thoughtram.io/git/2014/11/18/the-anatomy-of-a-git-commit.html)


# Supprimer un fichier du dépôt Git

On ajoute un fichier et on le committe
```bash
$ touch myfile.txt
$ git add myfile.txt
$ git status
$ git commit -m "added myfile"
$ git status
$ git log
```

Pour supprimer un fichier du dépôt : `git rm` à la place de `git add` puis `commit`:
```bash
$ git rm myfile.txt
$ git status
$ git commit -m "remove myfile"
$ git status
$ git log
```

Exemple de résultat de la commande `git log`:
```bash
commit f4e19dbcc02a31f0430e7ace2b7cf50581089a3f (HEAD -> master)
Author: Yohan Lascombe <ylascombe@octo.com>
Date:   Mon Dec 16 00:01:31 2019 +0100

    remove myfile

commit 9fa59ea3f539fbc31aa1fdda9ec1c7ab46156975
Author: Yohan Lascombe <ylascombe@octo.com>
Date:   Mon Dec 16 00:00:34 2019 +0100

    added myfile

commit 99b0bcb0b334123005aba2bdba858970c88abf1b
Author: Yohan Lascombe <ylascombe@octo.com>
Date:   Sun Dec 15 23:55:05 2019 +0100

    added read me
```

> Question avancée
>
> Comment supprimer un fichier du repo. sans le retirer du dossier de travail ?
>
> Réponse :
> [https://git-scm.com/docs/git-rm](https://git-scm.com/docs/git-rm)

## Ignorer un fichier ou un dossier pour ne pas l’envoyer dans le dépôt

Ajoutez un fichier `secret.json` à la racine du repo.

Vérifiez via `git status` que le fichier `secret.json` apparait dans la liste des fichiers non suivis :

```bash
$ git status
...

Fichiers non suivis:
  (utilisez "git add <fichier>..." pour inclure dans ce qui sera validé)

	secret.json

aucune modification n'a été ajoutée à la validation (utilisez "git add" ou "git commit -a")
```

Ajoutez un fichier `.gitignore` à la racine du repo.

Vous pouvez employer les syntaxes suivantes :

- Ignorer les fichiers *.pyc dans tout le repo : `**.pyc`
- Ignorer les fichiers *.pyc dans le dossier racine : `*.pyc`
- Ignorer tous les fichiers ou dossier sous le dossier src/data : `src/data`
- Ignorer tous les fichiers ou dossier appelés app : `app`

> Question :
>
> Ajouter maintenant une règle pour ignorer le fichier `secret.json`
>
> Réponse
> ```
> cat .gitignore
> secret.json
> ```

Vérifiez via `git status` que le fichier `secret.json` est ignoré (il n'apparait plus dans le résultat).

On ajoute le fichier `.gitignore` et on le committe :

```bash
$ git add .gitignore
$ git status
$ git commit -m "Add .gitignore"
$ git status
$ git log
```


# Envoi de votre dépôt en local sur gitlab.com

Depuis votre navigateur web, sur votre compte au sein de gitlab.com, cliquer sur le bouton <img src="./.assets/create-project-button.png" alt="Register gitlab" height="30px"/>

![](./.assets/create-repo-gitlab.png)

Vous devez obtenir une page similaire à celle-ci :
![](./.assets/project-created.png)


Comme indiqué par Gitlab, il ne vous reste plus qu'à envoyer (`git push`) votre dépôt avec les commandes suivantes.
Pensez à remplacer `<your username>` par votre username gitlab :

```
# Push an existing Git repository
$ git remote add origin git@gitlab.com:<your username>/monrepo.git
$ git push -u origin --all
$ git push -u origin --tags`
```

En exécutant ces commandes, vous obtiendrez probablement l'erreur suivante
```bash
git push -u origin --all
Warning: Permanently added 'gitlab.com,35.231.145.151' (ECDSA) to the list of known hosts.
Enumerating objects: 6, done.
Counting objects: 100% (6/6), done.
Delta compression using up to 8 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (6/6), 514 bytes | 514.00 KiB/s, done.
Total 6 (delta 1), reused 0 (delta 0)
To gitlab.com:ylascombe-octo/monrepo.git
 * [new branch]      master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.
```

# Création d'un fichier README, commit et push

Comme tout projet qui se respecte, nous allons maintenant créer un fichier `README.md` à la racine de notre projet, le commiter puis l'envoyer sur le dépôt Gitlab.

## Création du fichier README

```bash
$ cat <<EOF > README.md
Ceci est mon premier projet de test.
EOF
```

## Commit du fichier

```bash
$ git add README.md
$ git commit -m "doc: Ajout README"
```

## git push

```bash
$ git push origin master
```

> La réalisation de modifications directement sur la branche `master` que nous faisons ici n'est pas une bonne pratique.

Vous pouvez maintenant aller visualiser vos modifications sur Gitlab.

Ce TP est maintenant terminé.
